FROM scratch

MAINTAINER Tony Roy <tonyroy@wcksoft.com>

ADD openwrt-18.06.1-x86-generic-generic-rootfs.tar.gz /

RUN mkdir /var/lock

USER root
CMD /sbin/init
